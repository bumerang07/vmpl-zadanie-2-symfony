<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\Category;
use App\Form\BookType;
use App\Repository\BookRepository;
// use App\Repository\CategoriesRepository;
// use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Handler\DownloadHandler;

/**
 * @Route("/book")
 */
class BookController extends AbstractController
{
    // message that will be returned with resoults
    // i will store flash message here
    protected $message = null;

    /**
     * function that assign flash messages if there are any
     * to message variable which is returned to front in JSON
     */
    protected function setMessage()
    {
        $flashbag = $this->get('session')->getFlashBag();
        if( $flashbag && count( $flashbag->keys() )){
            $this->message = $flashbag->all();
        }
    }

    /**
     * @Route("/", name="book_index", methods={"GET"})
     */
    public function index(Request $request, BookRepository $bookRepository, SerializerInterface $serializer): Response
    {
        // check if it's an ajax request
        if ($request->isXMLHttpRequest()) {
            
            //get pagination variables
            // if those are not set, then just set dummy variables
            $page = $request->get('page') ?? 0;
            $perPage = $request->get('per_page') ?? 5;

            // paginate books 
            // lets get route variables
            $booksPaginated = $bookRepository->findBy([], null, $perPage, $page*$perPage);

            // count all books
            $booksCount = $bookRepository->createQueryBuilder('a')
                ->select('count(a.id)')
                ->getQuery()
                ->getSingleScalarResult();

            // need to generate and return insert token to allow adding books
            $intention = 'book';
            $csrf = $this->get('security.csrf.token_manager');
            $token = $csrf->getToken($intention);

            $this->setMessage();

            // need to use serializer to prevent errors 
            // when categories are returned with books
            // and code run into a loop
            $books = $serializer->serialize(
                $booksPaginated, 
                'json',
                ['groups' => 'list_books']
            );

            // return json response
            return $this->json([
                // i need to decode serialized categories, otherwise those are
                // returned as string
                'books' => json_decode($books),
                'newBookToken' => $token,
                'message' => $this->message,
                'booksCount' => $booksCount,
            ]);
        }
        return $this->render('book/index.html.twig', [
            'books' => $bookRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="book_new", methods={"GET","POST"})
     */
    public function new(Request $request, SerializerInterface $serializer): Response
    {
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);


        $intention = 'book';
        $csrf = $this->get('security.csrf.token_manager');

        $errors = [];
        $submitedToken = false;
        if ($form->isSubmitted()){
            $submitedToken = $request->request->get('_token');
            $submitedToken = $this->isCsrfTokenValid($intention, $submitedToken);
            $errors = $form->getErrors();
        }

        if (
            $form->isSubmitted() && $form->isValid() ||
            $form->isSubmitted() && !count($errors) && $submitedToken
            ) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Book was added!'
            );
            return $this->redirectToRoute('book_index');
        } elseif ($form->isSubmitted()) {
            $this->message = ['error' => 'Error during adding book'];
        }


        $token = $csrf->refreshToken($intention);

        if ($request->isXMLHttpRequest()) {      
            
            // get categories repository to get all categories
            $categoriesRepository = $entityManager = $this->getDoctrine()->getManager()->getRepository(Category::class);
            $allCategories = $categoriesRepository->findAll();


            $allCategories = json_decode($serializer->serialize(
                $allCategories, 
                'json',
                ['groups' => 'list_categories']
            ));

            $book = $serializer->serialize(
                $book, 
                'json',
                ['groups' => 'show_book']
            );
            return $this->json([
                'book' => $book,
                '_token' => $token->getValue(),
                'errors' => $errors,
                'submitedToken' => $submitedToken,
                'allCategories' => $allCategories,
            ]);
        }


        return $this->render('book/new.html.twig', [
            'book' => $book,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="book_show", methods={"GET"})
     */
    public function show(Request $request, Book $book, SerializerInterface $serializer, DownloadHandler $downloadHandler): Response
    {
        $image = $downloadHandler->downloadObject($book, 'imageFile');
        dump ($image);
        if ($request->isXMLHttpRequest()) {         
            $book = $serializer->serialize(
                $book, 
                'json',
                ['groups' => 'show_book']
            );
            return $this->json([
                'book' => json_decode($book),
            ]);
        }
        return $this->render('book/show.html.twig', [
            'book' => $book,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="book_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Book $book, SerializerInterface $serializer): Response
    {
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        $intention = 'book';
        $errors = [];
        $submitedToken = false;
        if ($form->isSubmitted()){
            $submitedToken = $request->request->get('_token');
            $submitedToken = $this->isCsrfTokenValid($intention, $submitedToken);
            $errors = $form->getErrors();
        }
        if (
            $form->isSubmitted() && $form->isValid() ||
            $form->isSubmitted() && !count($errors) && $submitedToken
            ) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Book was edited!'
            );

            return $this->redirectToRoute('book_index');
        }

        dump($book);

        $csrf = $this->get('security.csrf.token_manager');
        $token = $csrf->refreshToken($intention);

        $intention = 'delete'.$book->getId();
        $csrf = $this->get('security.csrf.token_manager');
        $token_delete = $csrf->refreshToken($intention);

        if ($request->isXMLHttpRequest()) {         

            // get categories repository to get all categories
            $categoriesRepository = $entityManager = $this->getDoctrine()->getManager()->getRepository(Category::class);
            $allCategories = $categoriesRepository->findAll();


            $allCategories = json_decode($serializer->serialize(
                $allCategories, 
                'json',
                ['groups' => 'list_categories']
            ));

            $book = $serializer->serialize(
                $book, 
                'json',
                ['groups' => 'show_book']
            );
            return $this->json([
                'book' => json_decode($book),
                '_token' => $token->getValue(),
                '_token_delete' => $token_delete->getValue(),
                'errors' => $errors,
                'submitedToken' => $submitedToken,
                'allCategories' => $allCategories,
            ]);
        }

        return $this->render('book/edit.html.twig', [
            'book' => $book,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="book_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Book $book): Response
    {
        if ($this->isCsrfTokenValid('delete'.$book->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($book);
            $entityManager->flush();
        }

        return $this->redirectToRoute('book_index');
    }
}
