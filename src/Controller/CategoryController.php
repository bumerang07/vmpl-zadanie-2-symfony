<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @Route("/category")
 */
class CategoryController extends AbstractController
{
    
    // message that will be returned with resoults
    // i will store flash message here
    protected $message = null;

    /**
     * function that assign flash messages if there are any
     * to message variable which is returned to front in JSON
     */
    protected function setMessage()
    {
        $flashbag = $this->get('session')->getFlashBag();
        if( $flashbag && count( $flashbag->keys() )){
            $this->message = $flashbag->all();
        }
    }

    /**
     * @Route("/", name="category_index", methods={"GET"})
     */
    public function index(Request $request, CategoryRepository $categoryRepository, SerializerInterface $serializer): Response
    {
        // check if it's an ajax request
        if ($request->isXMLHttpRequest()) {         
           
            //get pagination variables
            // if those are not set, then just set dummy variables
            $page = $request->get('page') ?? 0;
            $perPage = $request->get('per_page') ?? 5;

            // paginate categories 
            // lets get route variables
            $categoriesPaginated = $categoryRepository->findBy([], null, $perPage, $page*$perPage);

            // count all categories
            $categoriesCount = $categoryRepository->createQueryBuilder('a')
                ->select('count(a.id)')
                ->getQuery()
                ->getSingleScalarResult();

            

            // need to use serializer to prevent errors 
            // when books are returned with categories
            // and code run into a loop
            $categories = $serializer->serialize(
                $categoriesPaginated, 
                // $categoryRepository->findAll(), 
                'json',
                ['groups' => 'list_categories']
            );

            // need to generate and return insert token to allow adding categories
            $intention = 'category';
            $csrf = $this->get('security.csrf.token_manager');
            $token = $csrf->getToken($intention);

            $this->setMessage();

            // return json response
            return $this->json([
                // i need to decode serialized categories, otherwise those are
                // returned as string
                'categories' => json_decode($categories),
                'newCategoryToken' => $token,
                'message' => $this->message,
                'categoriesCount' => $categoriesCount,
            ]);
        }
        return $this->render('category/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="category_new", methods={"GET","POST"})
     */
    public function new(Request $request, SerializerInterface $serializer): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        $intention = 'category';
        $csrf = $this->get('security.csrf.token_manager');

        $currentToken = $csrf->getToken($intention);

        $errors = [];
        $submitedToken = false;
        if ($form->isSubmitted()){
            $submitedToken = $request->request->get('_token');
            $submitedToken = $this->isCsrfTokenValid($intention, $submitedToken);
            $errors = $form->getErrors();
        }
        
        if (
            $form->isSubmitted() && $form->isValid() ||
            $form->isSubmitted() && !count($errors) && $submitedToken
            ) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Category was added!'
            );
            return $this->redirectToRoute('category_index');
        } elseif ($form->isSubmitted()) {
            $this->message = ['error' => 'Error during adding category'];
        }

        
        // $token = $csrf->refreshToken($intention);

        $token = $csrf->getToken($intention);

        if ($request->isXMLHttpRequest()) {         
            $category = $serializer->serialize(
                $category, 
                'json',
                ['groups' => 'show_category']
            );
            return $this->json([
                'category' => $category,
                '_token' => $token->getValue(),
                'errors' => $errors,
                'message' => $this->message,
                'submitedToken' => $submitedToken,
                'currentToken' => $currentToken,
            ]);
        }


        return $this->render('category/new.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_show", methods={"GET"})
     */
    public function show(Request $request, Category $category, SerializerInterface $serializer): Response
    {
        if ($request->isXMLHttpRequest()) {         
            $category = $serializer->serialize(
                $category, 
                'json',
                ['groups' => 'show_category']
            );
            return $this->json([
                'category' => json_decode($category),
            ]);
        }
        return $this->render('category/show.html.twig', [
            'category' => $category,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="category_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Category $category, SerializerInterface $serializer): Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        
        $message = '';

        $intention = 'category';
        $errors = [];
        $submitedToken = false;
        if ($form->isSubmitted()){
            $submitedToken = $request->request->get('_token');
            $submitedToken = $this->isCsrfTokenValid($intention, $submitedToken);
            $errors = $form->getErrors();
        }
        if (
            $form->isSubmitted() && $form->isValid() ||
            $form->isSubmitted() && !count($errors) && $submitedToken
            ) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                'Category was edited!'
            );

            return $this->redirectToRoute('category_index');
        }

        $csrf = $this->get('security.csrf.token_manager');
        $token = $csrf->refreshToken($intention);

        $intention = 'delete'.$category->getId();
        $csrf = $this->get('security.csrf.token_manager');
        $token_delete = $csrf->refreshToken($intention);

        if ($request->isXMLHttpRequest()) {         
            $category = $serializer->serialize(
                $category, 
                'json',
                ['groups' => 'show_category']
            );
            return $this->json([
                'category' => json_decode($category),
                '_token' => $token->getValue(),
                '_token_delete' => $token_delete->getValue(),
                'errors' => $errors,
                'submitedToken' => $submitedToken,
            ]);
        }

        return $this->render('category/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Category $category): Response
    {
        if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($category);
            $entityManager->flush();
        }

        return $this->redirectToRoute('category_index');
    }
}
